import "./App.css";
import WeatherCardToday from "./Components/WeatherCardToday/WeatherCardToday";
import ForecastTable from "./Components/ForecastTable/ForecastTable";
import ForecastCard from "./Components/ForecastCard/ForecastCard";
import { useState, useEffect } from "react";

function App() {
  const placeholder = "Please Enter City Name";
  const [place, setPlace] = useState();
  const [forecast, setForecast] = useState();
  const [climate, setClimate] = useState([]);
  const [data, setData] = useState()

  const placeHanlder = (e) => {
    setPlace(e.target.value);
  };

  useEffect(() => {
    getServerData();
  }, []);

  const handleSubmit = (e) => {
    getServerData();
    e.preventDefault();
  };

  const getServerData = async () => {
    const url = `https://api.weatherapi.com/v1/forecast.json?key=0b81c00069be4d3fa7262132220408&q=${place? place: "hyderabad"}&days=7&aqi=no&alerts=no`;
    const option = {
      method: "GET",
    };
    try {
      const response = await fetch(url, option);
      const data = await response.json();
      setData(data)
      setForecast(data.forecast);
      let climateData = [];
      climateData.push(data.forecast.forecastday[0]);
      climateData.push(data.location);
      setClimate(climateData);
    } catch {
      const msg = "Unable to Connect the Server";
    }
  };

  return (
    <div className="weather-forecast-app">
      <div className="input-field-div">
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            value={place}
            className="input-field"
            placeholder={placeholder}
            onChange={placeHanlder}
            onSubmit={handleSubmit}
          />
        </form>
      </div>
      {forecast !== undefined ? (
        <>
          <ForecastCard data={data}/>
          <WeatherCardToday climate={climate} />
          <ForecastTable forecast={forecast} />
        </>
      ):
      (
        <h1>No matching location found.</h1>
      )}
    </div>
  );
}

export default App;
