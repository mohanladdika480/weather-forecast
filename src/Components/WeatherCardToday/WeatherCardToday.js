import "./WeatherCardToday.scss";
import { useEffect } from "react";
import { useState } from "react";

const WeatherCardToday = (props) => {
    const [place,setPlace] = useState()
    const [climateDet, setClimateDet] = useState()
    useEffect(() => {
        if (props) {
          const climate = props.climate;
          setClimateDet(climate[0])
          if(climate[1]) {
            setPlace(climate[1].name);
          }
        }
      });
  return (
    <div className="weather-card-today-container">
      <h1 className="todays-weather-heading"> Today's Weather in {place}</h1>
      <div className="weather-card">
        <table className="table-data">
          <tbody>
            <tr>
              <td className="data1"> Sunrise {climateDet && climateDet.astro && climateDet.astro.sunrise}</td>
              <td className="data3">Moonrise {climateDet && climateDet.astro && climateDet.astro.moonrise}</td>
              <td className="data2">Max.</td>
              <td className="data2">Min.</td>
              <td className="data2">Avg.</td>
              <td className="data2">Precip.</td>
              <td className="data2">Max.Wind</td>
            </tr>
            <tr>
              <td className="data1">Sunset {climateDet && climateDet.astro && climateDet.astro.sunset}</td>
              <td className="data3">Moonset {climateDet && climateDet.astro && climateDet.astro.moonset}</td>
              <td className="data2">{climateDet && climateDet.day && climateDet.day.maxtemp_f} &deg;F</td>
              <td className="data2">{climateDet && climateDet.day && climateDet.day.mintemp_f} &deg;F</td>
              <td className="data2">{climateDet && climateDet.day && climateDet.day.avgtemp_f} &deg;F</td>
              <td className="data2">{climateDet && climateDet.day && climateDet.day.totalprecip_mm} mm</td>
              <td className="data2">{climateDet && climateDet.day && climateDet.day.maxwind_kph} kph</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default WeatherCardToday;
