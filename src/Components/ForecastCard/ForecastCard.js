import "./ForecastCard.scss";
import { useState, useEffect } from "react";

const ForecastCard = (props) => {
  const [condition, setCondition] = useState()
  const [location, setLocation] = useState()
  const [units, setUnits] = useState("F")
  const [forecast, setForecast] = useState()
  const [today, setToday] = useState()
  useEffect(() => {
    if (props && props.data) {
      setCondition(props.data.current)
      setLocation(props.data.location)
      setForecast(props.data.forecast.forecastday)
      const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
      const date = new Date(props.data.current.last_updated)
      setToday(weekday[date.getDay()])
    }
  })
  const handleUnits = (e) => {
    setUnits(e)
    console.log(condition)
  }
  return (
    <div className="forecast-card-container">
      <div className="places-section">
        <div className="city-name">
          {location && location.name} Weather Forecast{" "}
          <span className="state-country">{location && location.region}, {location && location.country}</span>
        </div>
        <div className="right-div">
          <span className={`span ${units === "F" ? "active1":""}`} onClick={(e) => handleUnits("F")}> &deg;F</span>
          <span className={`span ${units === "C" ? "active2":""}`} onClick={(e) => handleUnits("C")}> &deg;C</span>
        </div>
      </div>
      <div className="forecast-banner">
        <div className="today-weather">
          <div className="temp-cloud">
            <div className="icon-div">
            <img src={condition && condition.condition && condition.condition.icon} alt="cloud-icon" className="cloud-icon" />
            </div>
            <div className="temp-and-condition">
              <span className="today-temp"> {condition && (units==="F"? condition.temp_f : condition.temp_c)} &deg;{units}  {today}</span>
              <span className="condition">{condition && condition.condition && condition.condition.text}</span>
            </div>
            <div className="wind-pressure">
              <span className="wind"> Wind </span>
              <span className="wind">{condition && (units==="F"? `${condition.wind_mph} mph` : `${condition.wind_kph} kmph`)} </span>
            </div>
            <div className="temp-and-condition">
              <span className="wind"> Precip </span>
              <span className="wind">{condition && (units==="F"? `${condition.precip_in} in` : `${condition.precip_mm} mm`)} </span>
            </div>
            <div className="temp-and-condition">
              <span className="wind"> Pressure </span>
              <span className="wind">{condition && (units==="F"? `${condition.pressure_in} in` : `${condition.pressure_mb} mb`)} </span>
            </div>
          </div>
        </div>
        <div className="week-all-conditions">
          {forecast && forecast.slice(1,).map((data)=> {
            const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
            const date = new Date(data.date)
            const week = weekday[date.getDay()]
            return (
            <div className="each-day">
            <img src={data.day.condition.icon} alt="cloud-icon" />
            <span>{week}</span>
            <span>{(units==="F"? `${data.day.avgtemp_f }` : `${data.day.avgtemp_c}`)} &deg;{units}</span>
            <span>{data.day.condition.text}</span>
          </div>)
          })}
        </div>
      </div>
    </div>
  );
};

export default ForecastCard;
