import "./ForecastTable.scss";
import GridViewIcon from "@mui/icons-material/GridView";
import SsidChartIcon from "@mui/icons-material/SsidChart";
import { useEffect } from "react";
import { useState } from "react";

const ForecastTable = (props) => {
  const [forecast, setForecast] = useState();
  const [count, setCount] = useState(0);
  const [selectedWeek, setSelectedWeek] = useState()
  const weekday = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
  useEffect(() => {
    if (props) {
      const forecast = props.forecast;
      setForecast(forecast);
    }
    const date = new Date(props.forecast.forecastday[0].date)
    setSelectedWeek(weekday[date.getDay()])
  },[]);

  const handleSelectDate = (e, week) => {
    setCount(e)
    setSelectedWeek(week)
  }

  return (
    <div className="forecast-contianer">
      <div className="weeks-and-units-div">
        <div className="weeks-div">
          {forecast && forecast.forecastday.map((data, index) => {
            const date = new Date(data.date)
            const week = weekday[date.getDay()]
            return (
              <>
              <span className={`span ${(index === count && (index === 0 && 'active1' || index === 6 && 'active2' || index !== 0 && index !== 6 && 'active'))}`} onClick={(e) => handleSelectDate(index, week)}> {week}</span>
              {forecast.forecastday.length !== (index + 1) && <span className="border-span"></span>}
              </>
            )
          })}
        </div>
        <div className="forecast-data-types">
          <GridViewIcon className="icon-style active" />
          <span className="border-span"></span>
          <SsidChartIcon className="icon-style" />
        </div>
      </div>
      <div className="table">
        <table className="table-border">
          <tbody>
            <tr className="table-row">
              <th className="table-heading">{selectedWeek}</th>
              <th className="table-heading">Icon</th>
              <th className="table-heading">Temp</th>
              <th className="table-heading">Wind</th>
              <th className="table-heading">Precip.</th>
              <th className="table-heading">Cloud</th>
              <th className="table-heading">Humidity</th>
              <th className="table-heading">Pressure</th>
            </tr>
            {forecast &&
              forecast.forecastday &&
              forecast.forecastday[count].hour.map((data, index) => {
                  const date = new Date(data.time)
                  const time = data.time.slice(11,)
                  const lcTime = date.toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })
                  if (time === "00:00" || time === "03:00" || time === "06:00" || time === "09:00" || time === "12:00" || time === "15:00" || time === "18:00" || time === "20:00")
                return (
                  <tr className="table-row">
                    <td className="table-time">{lcTime}</td>
                    <td className="table-data">
                      <img src={data.condition.icon} alt="cloud" />
                    </td>
                    <td className="table-data">{data.temp_f} &deg;F</td>
                    <td className="table-data">{data.wind_mph} mph</td>
                    <td className="table-data">{data.precip_in} in</td>
                    <td className="table-data">{data.cloud}%</td>
                    <td className="table-data">{data.humidity}%</td>
                    <td className="table-data">{data.pressure_in} in</td>
                  </tr>
                );
              })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default ForecastTable;
